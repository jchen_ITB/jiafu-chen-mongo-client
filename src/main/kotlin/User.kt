import io.realm.kotlin.ext.realmListOf
import io.realm.kotlin.types.RealmInstant
import io.realm.kotlin.types.RealmList
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.Index
import io.realm.kotlin.types.annotations.PrimaryKey
import org.bson.BsonObjectId

import org.mongodb.kbson.ObjectId
import java.util.Date
import java.util.Objects
import java.util.UUID

class User : RealmObject {
    @PrimaryKey
    var _id: ObjectId? = ObjectId()

    @Index
    var owner_id: String? = ""
    var email: String = ""
    var username: String? = ""
    var description: String? = ""
    var avatar: ByteArray = byteArrayOf()
    var updateDataTime: Date = Date()
    var createDateTime: Date = Date()
    var followers : List<BsonObjectId> = listOf()
    var following : List<BsonObjectId> = listOf()
}
