import io.realm.kotlin.ext.realmListOf
import io.realm.kotlin.types.RealmInstant
import io.realm.kotlin.types.RealmList
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.Index
import io.realm.kotlin.types.annotations.PrimaryKey
import org.bson.BsonObjectId
import org.mongodb.kbson.ObjectId
import java.util.Date

class Post() : RealmObject {

    @PrimaryKey
    var _id : ObjectId = ObjectId()

    @Index
    var owner_id : String = ""
    var title : String = ""
    var description : String = ""
    var image : ByteArray = byteArrayOf()
    var longitude : Double = 0.0
    var latitude : Double = 0.0
    var updateDataTime : Date = Date()
    var createDateTime : Date = Date()
    var likes : List<BsonObjectId> = listOf()
}