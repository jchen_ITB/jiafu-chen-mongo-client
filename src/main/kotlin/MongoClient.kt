import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings
import com.mongodb.ServerApi
import com.mongodb.ServerApiVersion
import com.mongodb.client.MongoClients
import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.Filters
import com.mongodb.client.model.Updates
import io.realm.kotlin.ext.isFrozen
import org.bson.Document
import org.bson.codecs.configuration.CodecRegistries.fromProviders
import org.bson.codecs.configuration.CodecRegistries.fromRegistries
import org.bson.codecs.pojo.PojoCodecProvider
import java.util.*

object MongoDB {
    private const val DATABASE_NAME = "spothub-dev"
    private const val USER_COLLECTION = "User"
    private const val POST_COLLECTION = "Post"

    private lateinit var db : MongoDatabase
    private lateinit var userCollection : MongoCollection<User>
    private lateinit var postCollection : MongoCollection<Post>
    private val scanner : Scanner = Scanner(System.`in`)

    fun start(){
        val connection = "mongodb+srv://jchen:jchen@jiafu-chen-spothub-clus.bcydtag.mongodb.net/?retryWrites=true&w=majority";
        val serverAPI = ServerApi.builder()
            .version(ServerApiVersion.V1)
            .build()
        val mongoClientSetting = MongoClientSettings.builder()
            .applyConnectionString(ConnectionString(connection))
            .serverApi(serverAPI)
            .build()
        val pojoCodecProvider = PojoCodecProvider.builder().automatic(true).build()
        val pojoCodecRegistry = fromRegistries(
            MongoClientSettings.getDefaultCodecRegistry(),
            fromProviders(pojoCodecProvider)
        )

        try{
            MongoClients.create(mongoClientSetting).use {
                db = it.getDatabase(DATABASE_NAME).withCodecRegistry(pojoCodecRegistry)
                db.runCommand(Document("ping", 1))
                println("Pinged your deployment. You successfully connected to MongoDB!")

                userCollection = db.getCollection(USER_COLLECTION, User::class.java)
                postCollection = db.getCollection(POST_COLLECTION, Post::class.java)

                var input : Int

                println("""
                    ##############################################################
                    ###################### SPOTHUB DATABASE ######################
                    ##############################################################
                """.trimIndent())

                do {
                    println("""
                        Database : ${db.name}
                        Collections:
                            0 - Exit
                            1 - User collection
                            2 - Post collection
                    """.trimIndent())

                    print("Choose a collection to work with: ")
                    input = scanner.nextInt()

                    when (input) {
                        0 ->{
                            println("Exiting...")
                            break
                        }
                        1 -> enterUserCollection()
                        2 -> enterPostCollection()
                        else -> println("Invalid input!")
                    }
                } while (input != 0)

            }


        }catch (e: Exception){
            e.printStackTrace()
        }
    }

    private fun enterPostCollection() {
        var input : Int
        println("""
                ###################### POST COLLECTION ######################
            """.trimIndent())

        do {
            println("""
                        Database : ${db.name}
                        Operations:
                            0 - Exit
                            1 - Get all posts
                            2 - Create new post
                            3 - Update a post
                            4 - Delete a post
                    """.trimIndent())

            print("Choose an operation to perform: ")
            input = scanner.nextInt()

            when (input) {
                0 ->{
                    println("Exiting...")
                    break
                }
                1 -> getPosts()
                2 -> createPost()
                3 -> updatePost()
                4 -> deletePost()
                else -> println("Invalid input!")
            }
        } while (input != 0)
    }
    private fun enterUserCollection() {
        var input : Int
        println("""
                ###################### USER COLLECTION ######################
            """.trimIndent())

        do {
            println("""
                        Database : ${db.name}
                        Operations:
                            0 - Exit
                            1 - Get all users
                            2 - Create new user
                            3 - Update an user
                            4 - Delete an user
                    """.trimIndent())

            print("Choose an operation to perform: ")
            input = scanner.nextInt()

            when (input) {
                0 ->{
                    println("Exiting...")
                    break
                }
                1 -> getUsers()
                2 -> createUser()
                3 -> updateUser()
                4 -> deleteUser()
                else -> println("Invalid input!")
            }
        } while (input != 0)
    }

    //region POST COLLECTION CRUD
    private fun getPosts() {
        while (true){
            println("\nListing all posts...\n")
            val posts = postCollection.find()

            posts.forEachIndexed{ index, post ->
                println("${index+1} - ${post.title}")
            }

            print("\nChoose a post to see more details (0 to exit): ")
            var index : Int = scanner.nextInt()

            if(index <= 0) break

            println("Loading post details...\n")

            val post = posts.elementAt(index-1)
            println("POST ${index} DETAILS :")
            printPost(post)

            print("Want to see another post? Y/N: ")
            when(scanner.next().uppercase()){
                "Y" -> continue
                "N" -> break
                "" -> break
            }
        }
    }
    private fun createPost() {
        val post = Post()
        scanner.nextLine()
        print("Enter a title: \n")
        post.title = scanner.nextLine()

        print("Enter a description: \n")
        post.description = scanner.nextLine()

        var longitude : Double? = null
        while (longitude == null){
            print("Enter a longitude: \n")
            longitude = scanner.next().toDoubleOrNull()
            if(longitude == null){
                println("Invalid longitude!")
            }else{
                post.longitude = longitude
            }
        }

        var latitude : Double? = null
        while (latitude == null){
            print("Enter a latitude: \n")
            latitude = scanner.next().toDoubleOrNull()
            if(latitude == null){
                println("Invalid latitude!")
            }else{
                post.latitude = latitude
            }
        }

        print("Are you sure you want to add this post? (Y/N): ")
        val confirm = scanner.next()

        if(confirm.uppercase() == "Y"){
            try {
                val result = postCollection.insertOne(post)
                println("Post updated! ID: ${result.insertedId}\n")
            }catch (e: Exception) {
                println("Failed to add post!\n")
                e.printStackTrace()
            }
        }
    }
    private fun updatePost() {
        println("Listing all posts...\n")
        val posts = postCollection.find()

        posts.forEachIndexed{ index, post ->
            println("${index+1} - ${post.title}")
        }

        print("\nChoose a post to update (0 to exit): ")

        var index : Int = scanner.nextInt()

        if(index <= 0) return

        println("Loading post details...\n")

        val post = posts.elementAt(index-1)
        println("\nPOST ${index} DETAILS :")
        printPost(post)

        print("Enter a new title (Empty to skip): \n")
        scanner.nextLine()
        var title = scanner.nextLine()
        if(title.isNullOrEmpty()) title = post.title

        print("Enter a new description (Empty to skip): \n")
        var description = scanner.nextLine()
        if(description.isNullOrEmpty()) description = post.description

        var longitude : Double? = null
        while (longitude == null){
            print("Enter a new longitude (Empty to skip): \n")
            val input = scanner.nextLine()
            if(input.isEmpty()) break
            longitude = input.toDoubleOrNull()
            if(longitude == null) println("Invalid longitude!")
        }

        if(longitude == null) longitude = post.longitude

        var latitude : Double? = null
        while (latitude == null){
            print("Enter a new latitude (Empty to skip): \n")
            val input = scanner.nextLine()
            if(input.isEmpty()) break
            latitude = input.toDoubleOrNull()
            if(latitude == null) println("Invalid latitude!")
        }

        if(latitude == null) latitude = post.latitude

        print("Are you sure you want to update this post? (Y/N): ")
        val confirm = scanner.next()

        if(confirm.uppercase() == "Y"){
            try {
                val filter = Filters.and(Filters.eq("title", post.title),Filters.eq("createDateTime", post.createDateTime))
                val updateObject = Updates.combine(
                    Updates.set("title", title),
                    Updates.set("description", description),
                    Updates.set("longitude", longitude),
                    Updates.set("latitude", latitude),
                    Updates.set("updateDataTime", Date())
                )

                val result = postCollection.updateOne(filter, updateObject)

                println("Post updated! Modified count:${result.modifiedCount}\n")
            }catch (e: Exception) {
                println("Failed to update post!\n")
                e.printStackTrace()
            }
        }

    }
    private fun deletePost() {
        println("Listing all posts...\n")
        val posts = postCollection.find()

        posts.forEachIndexed{ index, post ->
            println("${index+1} - ${post.title}")
        }

        print("\nChoose a post to delete it (0 to exit): ")

        var index : Int = scanner.nextInt()

        if(index <= 0) return

        println("Loading post details...\n")

        val post = posts.elementAt(index-1)
        println("POST ${index} DETAILS :")
        printPost(post)

        print("Are you sure you want to delete this post? (Y/N): ")
        val confirm = scanner.next()

        if(confirm.uppercase() == "Y"){
            try {
                val filter = Filters.and(Filters.eq("title", post.title),Filters.eq("createDateTime", post.createDateTime))
                val result = postCollection.deleteOne(filter)
                println("Post deleted! Delete count : ${result.deletedCount}\n")
            }catch (e: Exception) {
                println("Failed to delete post!\n")
                e.printStackTrace()
            }
        }

    }
    //endregion

    //region USER COLLECTION CRUD
    private fun getUsers(){
        while (true){
            println("\nListing all users...\n")
            val users = userCollection.find()

            users.forEachIndexed{ index, user ->
                println("${index+1} - ${user.username}")
            }

            print("\nChoose an user to see more details (0 to exit): ")
            var index : Int = scanner.nextInt()

            if(index <= 0) break

            println("Loading user number $index ...: \n")
            val user = users.elementAt(index-1)

            println("USER ${index} DETAILS :")
            printUser(user)

            print("Want to see another user? Y/N : ")
            when(scanner.next().uppercase()){
                "Y" -> continue
                "N" -> break
                "" -> break
            }
        }
    }
    private fun createUser(){
        val user = User()
        scanner.nextLine()
        print("Enter a username: \n")
        user.username = scanner.nextLine()

        print("Enter a description: \n")
        user.description = scanner.nextLine()

        print("Enter an email: \n")
        user.email = scanner.nextLine()

        print("Are you sure you want to add this user? (Y/N): ")
        val confirm = scanner.next()

        if(confirm.uppercase() == "Y"){
            try {
                val result = userCollection.insertOne(user)
                println("User added! ID: ${result.insertedId}\n")
            }catch (e: Exception) {
                println("Failed to add user!\n")
                e.printStackTrace()
            }
        }
    }
    private fun updateUser(){
        println("Listing all users...\n")
        val users = userCollection.find()

        users.forEachIndexed{ index, user ->
            println("${index+1} - ${user.username}")
        }

        print("\nChoose an user to update (0 to exit): ")

        var index : Int = scanner.nextInt()

        if(index <= 0) return

        println("Loading user details...\n")

        val user = users.elementAt(index-1)
        println("\nUSER ${index} DETAILS :")
        printUser(user)

        print("Enter a new username (Empty to skip): \n")
        scanner.nextLine()
        var username = scanner.nextLine()
        if(username.isNullOrEmpty()) username = user.username

        print("Enter a new description (Empty to skip): \n")
        var description = scanner.nextLine()
        if(description.isNullOrEmpty()) description = user.description

        print("Enter a new email address (Empty to skip): \n")
        var email = scanner.nextLine()
        if(email.isNullOrEmpty()) email = user.email

        print("Are you sure you want to update this user? (Y/N): ")
        val confirm = scanner.next()

        if(confirm.uppercase() == "Y"){
            try {
                val filter = Filters.and(Filters.eq("email", user.email),Filters.eq("createDateTime", user.createDateTime))
                val updateObject = Updates.combine(
                    Updates.set("username", username),
                    Updates.set("description", description),
                    Updates.set("email", email),
                    Updates.set("updateDataTime", Date())
                )

                val result = userCollection.updateOne(filter, updateObject)

                println("User updated! Modified count:${result.modifiedCount}\n")
            }catch (e: Exception) {
                println("Failed to update user!\n")
                e.printStackTrace()
            }
        }
    }
    private fun deleteUser(){
        println("Listing all posts...\n")
        val users = userCollection.find()

        users.forEachIndexed{ index, user ->
            println("${index+1} - ${user.username}")
        }

        print("\nChoose an user to delete it (0 to exit): ")

        var index : Int = scanner.nextInt()

        if(index <= 0) return

        println("Loading user details...\n")

        val user = users.elementAt(index-1)
        println("USER ${index} DETAILS :")
        printUser(user)

        print("Are you sure you want to delete this user? (Y/N): ")
        val confirm = scanner.next()

        if(confirm.uppercase() == "Y"){
            try {
                val filter = Filters.and(Filters.eq("email", user.email),Filters.eq("createDateTime", user.createDateTime))
                val result = userCollection.deleteOne(filter)
                println("User deleted! Delete count : ${result.deletedCount}\n")
            }catch (e: Exception) {
                println("Failed to delete user!\n")
                e.printStackTrace()
            }
        }
    }

    //endregion

    private fun printPost(post: Post){
        println()
        println("""
            ID : ${post._id}
            Owner ID : ${post.owner_id}
            Title : ${post.title}
            Description : ${post.description}
            Longitude : ${post.longitude}
            Latitude : ${post.latitude}
            Image ByteArray : ${post.image}
            Likes : ${post.likes.size}
            Create Date : ${post.createDateTime}
            Update Date : ${post.updateDataTime}
        """.trimIndent())
        println()
    }

    private fun printUser(user : User){
        println()
        println("""
            ID : ${user._id}
            Owner ID : ${user.owner_id}
            USERNAME : ${user.username}
            Description : ${user.description}
            EMAIL : ${user.email}
            FOLLOWERS : ${user.followers.size}
            FOLLOWING : ${user.following.size}
            PROFILE IMAGE : ${user.avatar}
            Create Date : ${user.createDateTime}
            Update Date : ${user.updateDataTime}
        """.trimIndent())
        println()
    }
}
